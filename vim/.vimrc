" inform vim of terminal type
set term=xterm

" set color scheme
colorscheme railscasts

" enable syntax highlight
syntax on

" enable line numbers
set number

" show file name in title bar
set title

" use OS X clipboard
set clipboard=unnamed

" set cursor line
set cursorline

" set tabs as wide as two spaces
set tabstop=2

" always show status line
set laststatus=2


